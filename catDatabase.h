///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
///
/// Usage: This C file will store cat data.
///   catData base will hold difference aspects of each cat such as
///   the cats weight, gender name and color
/// 
///
///
/// Compilation:
///
/// @file catDatabase.h
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdio.h>
#include <stdbool.h>


#define MAX_NAME_LEN 30 
#define MAX_CATS 10 

extern int     numCats;

enum           Gender {UNKNOWN_GENDER = 0, FEMALE =1 , MALE = 2 };
enum           Breed  {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};


//reference arrays declared in catDatabase.c
extern char           nameArray[ MAX_CATS ] [MAX_NAME_LEN ];
extern bool           isFixedArray[ MAX_CATS ];
extern float          weightArray[ MAX_CATS ];
extern enum Gender    genderArray[ MAX_CATS ];
extern enum Breed     breedArray [ MAX_CATS ];

//functions declared in catDatabase.c

extern bool validateCat       (  const char inputCatName[],
                                 const float inputCatWeight );
extern bool validateName      (  const char inputCatName[] );
extern bool validateWeight    (  const float inputCatWeight );
extern bool validateIndex     (  const int currentCat );
extern bool validateNumCats   ( ); 

extern void initializeCats();

