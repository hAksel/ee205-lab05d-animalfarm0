///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
///
///
///
/// @file deleteCats.h
///
///
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
///////////////////////////////////////////////////////////////////////////
#pragma once 

extern void deleteAllCats ( );

