///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
///
///
///
/// @file updateCats.c
//
///
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "updateCats.h"



bool updateCatName ( const  int currentCat, const char newCatName[] ){
   if (     validateName ( newCatName )
         || validateIndex ( currentCat ) == false){
      return false;
   }
   strcpy ( nameArray [ currentCat ], newCatName ); 
   return true; 
}

bool fixCat ( const int currentCat ){ 
   if ( validateIndex ( currentCat ) == false ){
      return false;
   }
   isFixedArray [ currentCat ] = true; 
   return true;
}

bool updateWeight ( const int currentCat, const float newWeight ){ 
   if ( validateWeight ( newWeight ) == false ){
      return false ;
   }
   weightArray [ currentCat ] = newWeight; 
  return true;
}

