///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
///
///
///
/// @file reportCats.c
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "catDatabase.h"
#include "reportCats.h" 

void printCat ( const int currentCat ){
   if ( currentCat < 0 || currentCat >= MAX_CATS ){ 
      printf("animalFarm0: Bad Cat [%d]\n", currentCat);
     return;
   }
   printf( "############\n" );
   printf( "current Cat [%i]\n",    currentCat );
   printf( "name = [%s]\n",         nameArray[ currentCat ] );
   printf( "gender = [%i]\n",       genderArray [currentCat ] );
   printf( "breed = [%i]\n",        breedArray [currentCat] );
   printf( "fixed = [%i]\n",        isFixedArray [currentCat] );
   printf( "weight = [%.2f]\n",     weightArray [currentCat] );
   printf( "$$$$$$$$$$$$\n" );
   return;
}

void printAllCats ( ){
   for ( int currentCat = 0; currentCat <  MAX_CATS; currentCat++ ){ 
      printf( "############\n" );
      printf( "current Cat [%i]\n",    currentCat );
      printf( "name = [%s]\n",         nameArray[ currentCat ] );
      printf( "gender = [%i]\n",       genderArray [currentCat ] );
      printf( "breed = [%i]\n",        breedArray [currentCat] );
      printf( "fixed = [%i]\n",        isFixedArray [currentCat] );
      printf( "weight = [%.2f]\n",     weightArray [currentCat] );  
      printf( "$$$$$$$$$$$$\n" );
   }
   return;
}

int findCat ( const char catName [] ){ 
   for ( int currentCat = 0; currentCat <  MAX_CATS; currentCat++ ){
      if ( strcmp ( nameArray [ currentCat ], catName ) == 0){
         printf( "[%s] is cat number [%i]\n", catName, currentCat);
         return currentCat;
      }
   }
   printf("error 425, this cat does not exist!\n");
   return 0;
}






