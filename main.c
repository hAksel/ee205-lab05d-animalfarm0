///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
///
/// Usage: This C file will define a function that can store cat data.
///   catData base will hold difference aspects of each cat such as
///   the cats weight, gender name and color
/// Intput: when calling this function:
//    catDatabase ( char array, enum , enum , bool , float ) 
/// 
///
///
/// Compilation:
///
/// @file catDatabase.c
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////

#include <stdio.h> 
#include <stdbool.h>
#include <string.h>
#include <assert.h> 
#include "catDatabase.h"
#include "addCats.h" 
#include "reportCats.h"
#include "updateCats.h" 
#include "deleteCats.h"

//#define DEBUG
#define TEST 

int main(){
   printf("Starting Animal Farm 0\n");
   //ADD CATS 
   #ifdef DEBUG
   printf( "number of cats numCat = [%i]\n", numCats);
   #endif
   
   #ifdef TEST
   printf("Adding cats\n");
   #endif 
   addCat( "Bob" , MALE, PERSIAN, true, 8.5 );
   //assert(  addCat( "Bob" , MALE, PERSIAN, true, 8.5 ) == true );

   assert ( addCat( "AlanBrilliant" , FEMALE, 
            PERSIAN, false, 152.1 ) == true );
   #ifdef DEBUG
   printf( "number of cats numCat = [%i]\n", numCats);
   #endif

   #ifdef TEST
   printf( "testing validation in addCat\n");
   //repeat name    
   assert(  addCat( "AlanBrilliant" , FEMALE, PERSIAN, 
            false, 152.1 ) ==  false ); 
   //no name
   assert(  addCat( "" , FEMALE, PERSIAN, 
            false, 152.1 ) == false);
   //long name 
   assert(  addCat( "AlanBrilliantisabrokeandignora" , 
            FEMALE, PERSIAN, false, 152.1 ) == false );
   //0 weight
   assert(  addCat( "VeryLightCat" , 
            FEMALE, PERSIAN, false, 0 ) == false );
   #endif 
   
   //good cats
   addCat( "DanusTorbalds" , MALE, SHORTHAIR, true, 9.6 );
   addCat( "SecretCat" , UNKNOWN_GENDER, UNKNOWN_BREED, true, 6.2 );
   addCat( "FemCat" , FEMALE, MANX, false, 9.7 );
   addCat( "Roxy" , FEMALE, MAINE_COON, true, 8.8 );
   addCat( "DavidGilmor" , MALE, SHORTHAIR, true, 7.7 );
   addCat( "RogerWaters" , MALE, SPHYNX,  false, 8.3 );
   addCat( "NickMason" , MALE, SPHYNX,  true, 8.7 );
   addCat( "RichardWright" , MALE, SPHYNX,  true, 6.3 );
   //extra cat
   #ifdef TEST 
   addCat( "ExtraCat" , MALE, SPHYNX,  true, 8.3 );
   #endif 

   //REPORT CATS 
   #ifdef TEST
   printf( "testing printCat and printAllCats\n");
   printCat ( 7 );  
   printAllCats(); 
   printf( "testing find cat\n" ); 
   findCat ("DavidGilmor"); 
   findCat ("deadCat"); 
   
   //UPDATE CATS
   printf( "testing updateCats\n"); 
   updateCatName ( 7 , "WogerRaters88");
   fixCat ( 7 );
   updateWeight ( 7, 0);
   updateWeight ( 7, 120.00 ); 
   printCat ( 7 ); 
   //duplicate name    
   updateCatName( 2, "DavidGilmor");
   //long name
   updateCatName( 1, "reallyreallyreallyreallyreallylongcatname" );
   //empty name 
   updateCatName( 2, "" );
   #endif

   //DELETE CATS
   #ifdef TEST
   printf( "testing delete all cats\n");
   deleteAllCats (); 
   printAllCats ();
   printf("now the number of cats is [%i]\n", numCats); 
   #endif 
   
   printf("Finished with Animal Farm 0\n");
      

}
