///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
///
///
///
/// @file deleteCats.c
//
///
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h> 

#include "catDatabase.h"
#include "deleteCats.h"





void deleteAllCats ( ){
   memset( nameArray, 0, sizeof(nameArray) ); 
   
   //memset( genderArray, 0, sizeof(genderArray) );
   for ( int currentCat = 0; currentCat < MAX_CATS; currentCat++){
      genderArray[ currentCat ] = 0; //kept in this way becuase 
                                     //I could set gender == 1 
                                     //without error
   }
   for ( int currentCat = 0; currentCat < MAX_CATS; currentCat++){
      breedArray[ currentCat ] = 0;
   }
   for ( int currentCat = 0; currentCat < MAX_CATS; currentCat++){
      isFixedArray[ currentCat ] = 0;
   }
   for ( int currentCat = 0; currentCat < MAX_CATS; currentCat++ ){
      weightArray[ currentCat ] = 0; 
   }
   
   numCats = 0; 
}
