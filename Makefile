###############################################################################
###
### @brief
###
### @file
### @version 1.0 - Initial version
###
### Build the animal farm (amish style)  
###
### @author  Aksel Sloan <aksel@hawaii.edu>  
### @date    14 February 2022 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC     = gcc
CFLAGS = -g -Wall -Wextra
TARGET = main
all: $(TARGET)


catDatabase.o: catDatabase.c catDatabase.h 
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h 
	$(CC) $(CFLAGS) -c main.c

main: main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o 

test: $(TARGET)
	 ./$(TARGET) 

clean:
	rm -f $(TARGET) *.o
