///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
///
/// Usage: This C file will store cat data.
///   catData base will hold difference aspects of each cat such as
///   the cats weight, gender name and color
/// 
///
///
/// Compilation: make or make test
///
/// @file catDatabase.c
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdbool.h> 
#include <string.h> 
#include "catDatabase.h" 
#define DEBUG

//array declaration and memory allocation
char           nameArray[ MAX_CATS ] [MAX_NAME_LEN ];
enum Gender    genderArray[ MAX_CATS ];
enum Breed     breedArray [ MAX_CATS ]; 
bool           isFixedArray[ MAX_CATS ];
float          weightArray[ MAX_CATS ];

int numCats = 0; 

bool validateName ( const char inputCatName[] ){
   //check for null 
   if ( strlen ( inputCatName ) < 1 ){
      printf( "error 421.5, cats must have a name!\n" );
      return false;
   }
   if ( strlen ( inputCatName ) >= MAX_NAME_LEN ){
      printf( "error 422.5, cat name is too long!\n" );
      return false;
   }
 
   int j;  
   for ( j = 0 ; j < MAX_CATS ; j++ ){
      if ( strcmp ( nameArray [ j ], inputCatName ) == 0 ){
         printf( "error 423.5, cats must have a unique name!\n" );
         return false;
      }
   }
   return true;
}


bool validateWeight ( const float inputCatWeight ){
   if ( inputCatWeight <= 0 ){
      printf( "error 424.5, cat weight must be > 0!\n" );
      return false;
   }
   return true;
}

bool validateIndex ( const int currentCat ){
   if ( currentCat >= MAX_CATS  || currentCat < 0 ){
      printf( "error 430, cat out of index range!\n" );
      return false;
   }
   return true;
}

bool validateNumCats ( ){
   if ( numCats >= MAX_CATS ){
      printf( "error 420.5, too many cats!\n" );
      return false;
   }
   return true;
}

bool validateCat (   const char inputCatName[],
                     const float inputCatWeight ){
   //number of cats validation 
   if ( numCats >= MAX_CATS ){
      printf( "error 420, too many cats!\n" );
      return false;
   }  
   //name validation 
   if ( strlen ( inputCatName ) < 1 ){
      printf( "error 421.5, cats must have a name!\n" ); 
      return false;
   }
   if ( strlen ( inputCatName ) >= MAX_NAME_LEN ){
      printf( "error 422.5, cat name is too long!\n" );
      return false;
   }

   int j; 
   for ( j = 0 ; j < MAX_CATS ; j++ ){
      if ( strcmp ( nameArray [ j ], inputCatName ) == 0 ){
         printf( "error 423.5, cats must have a unique name!\n" );
         return false;
      }
   }
   //weight validation
   if ( inputCatWeight <= 0 ){
      printf( "error 424.5, cat weight must be > 0!\n" );
      return false;
   }
   return true;
}

void initializeCats(){} //finish me 

void validateMe( ){
   printf( "you're doing great, hunny\n" ); 
}



