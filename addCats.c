///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - AnimalFarm0 - EE 205 - Spr 2022
///
///
///
/// @file addCats.c
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////
#include <stdio.h> 
#include <stdbool.h>
#include <string.h> 
#include "catDatabase.h"
#include "addCats.h"

//#define DEBUG
#define DEBUG2


bool addCat (  char catName[], 
               enum Gender catGender,  
               enum Breed catBreed,    
               bool catIsFixed, 
               float catWeight ){ 

   #ifdef DEBUG2
   printf("numCats = [%i]\n", numCats); 
   #endif
   
   if (  validateName( catName ) == false ) {       
      return false;
   }
   if ( validateWeight( catWeight ) == false ) { 
      return false;
   }
   if ( validateNumCats () == false ) {
      return false; 
   }

   strcpy( nameArray [ numCats ] , catName );
   #ifdef DEBUG
   printf( "the first letter is %c\n" , nameArray[ numCats ][ 0 ] );
   printf( "the next letter is %c\n" , nameArray[ numCats ][ 1 ] );
   #endif
   
   genderArray [ numCats ] = catGender; 
   #ifdef DEBUG 
   printf( "The gender is [%i]\n", genderArray[ numCats ] );  
   #endif 
   
   breedArray [ numCats ] = catBreed; 
   #ifdef DEBUG
   printf( "The breed is [%i]\n", breedArray [numCats] );
   #endif
   
   isFixedArray [ numCats ] = catIsFixed;
   #ifdef DEBUG
   printf( "isFixed = [%i] (0 is false)\n", isFixedArray [ numCats ]); 
   #endif

   weightArray [ numCats ] = catWeight; 
   #ifdef DEBUG 
   printf( "cat weight = [%.2f]\n" , weightArray [ numCats ]); 
   #endif
  
   numCats++; //numCats goes to 10 when cats 0-9 are added
              //note the 10th cat does not exist
   return true;
}

